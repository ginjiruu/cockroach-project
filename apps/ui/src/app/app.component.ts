import { Component, OnInit } from "@angular/core";
import { RouterModule } from "@angular/router";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { AsyncPipe, JsonPipe } from "@angular/common";

@Component({
  standalone: true,
  imports: [RouterModule, HttpClientModule, AsyncPipe, JsonPipe],
  selector: "cockroach-platform-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "ui";
  message = this.httpClient.get("api");

  constructor(private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    console.log("Starting");
  }
}
